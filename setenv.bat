@echo off
echo 'Backing up your old PATH to old_path.txt...'
echo %PATH% > ./old_path.txt
setx PATH "%PATH;%1"
echo 'PATH succesfully configured. Opening a new terminal and closing this one to reflect the updated PATH...'
start cmd
timeout /t 2 > NUL
exit 0
