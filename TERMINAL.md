## Helpful Terminal or DOS (aka cmd, command line) commands
#### Key: ml: macOS/Linux, w: Windows
* `.`: Refers to the working directory
* `..`: Refers to the parent directory
* `cd <dir_path>` (ml, w): **C**hange **d**irectory to the one specified by <dir_path>, which essentially means that you are 'opening' this folder in your terminal
* `ls` (ml) or `dir` (w): **L**i**s**t the files available in the current **dir**ectory
* `pwd` (ml): **P**rint **w**orking **d**irectory; you don't need this for Windows because your terminal should most likely show the full path at the prompt anyway
* `rm <file_path>` (ml) or `del <file_path>` (w): **R**e**m**ove or **del**ete the file specified by <file_path>
* `rm -rf <dir_path>` (ml) or `rmdir /s /q <dir_path>` (w): **R**e**m**ove the directory specified by <dir_path>, as well as all of the subdirectories and files which belong to it
* `<cmd_1> && <cmd_2> && ...` (ml, w): Chain <cmd_1>, <cmd_2>, **and** (&&) all following commands (minimum of 2 total) together, which saves you from typing the commands out on separate lines, but executes it the same way - one after the other  
**While you are typing out file/directory paths or some commands, you can press `TAB` to complete it for you *(if you have already typed out enough so that it can distinguish it from other files at that path or other commands)* so you don't have to type out the whole thing**
