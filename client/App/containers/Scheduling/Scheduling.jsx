import React, { Component } from 'react';
import {
  Calendar, Button, Input, AutoComplete
} from 'antd';
import moment from 'moment';
import styles from './Scheduling.css';
import GameInfoModal from './components/GameInfoModal';
import CreateGameModal from './components/CreateGameModal';

class Scheduling extends Component {
  static disabledDate(current) {
    const date = moment();
    date.hour(0);
    date.minute(0);
    date.second(0);
    return current.valueOf() < date.valueOf();
  }

  _isMounted = false;

  constructor(props) {
    super(props);

    const now = new Date(Date.now()).toUTCString();
    this.state = {
      value: moment(now),
      month: moment(now).month(),
      selectedValue: moment(now),
      visible: false,
      confirmLoading: false,
      game: '',
      selectedDay: '',
      selectedGame: '',
      players: [],
      alts: [],
      data: {},
      // users: [],
      usernames: [],
      createModal: false,
      okText: 'Join',
      showDelete: false,
      createPlayers: [],
      createAlts: [],
      createTime: moment(now),
      alertType: 'error',
      alertClosable: false,
      alertText: 'At least one player is required',
      alert: false,
      initial: true
    };
    this.onPanelChange = this.onPanelChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.showModal = this.showModal.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleCreateOk = this.handleCreateOk.bind(this);
    this.handleInfoOk = this.handleInfoOk.bind(this);
    this.dateCellRender = this.dateCellRender.bind(this);
    this.createGame = this.createGame.bind(this);
    this.updatePlayer = this.updatePlayer.bind(this);
    this.updateAlt = this.updateAlt.bind(this);
    this.updateTime = this.updateTime.bind(this);
    this.getMonthData = this.getMonthData.bind(this);
    this.onChange = this.onChange.bind(this);
    this.deleteGame = this.deleteGame.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getMonthData()
      .catch(console.error);

    this.getUsers()
      .catch(console.error);

    this.setStyle();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onSelect(value) {
    this.setState({
      value,
      selectedValue: value,
    });
    this.createGame(value);
  }

  onPanelChange(value) {
    this.setState({ value });
    setTimeout(this.setStyle, 1);
  }

  onChange(date) {
    const { month } = this.state;
    if (date.month() !== month) {
      this.setState({ month: date.month() });
      this.getMonthData();
    }
  }

  setStyle = () => {
    Array.from(document.getElementsByClassName('ant-select-selection__rendered'))
      .forEach(el => { el.style.display = 'flex'; el.style.alignItems = 'center'; }); // eslint-disable-line

    Array.from(document.getElementsByClassName('ant-fullcalendar-date'))
      .forEach(el => {
        el.style.backgroundColor = '#001529'; // eslint-disable-line
      });

    Array.from(document.getElementsByClassName('ant-fullcalendar-disabled-cell'))
      .forEach(el => {
        el.firstChild.style.backgroundColor = 'transparent'; // eslint-disable-line
      });
  };

  getUsers() {
    const mapToUsername = user => user.username.toUpperCase();
    const byAlpha = (a, b) => {
      if (a < b) return -1;
      if (a > b) return 1;
      return 0;
    };
    return new Promise((resolve, reject) => {
      fetch('/api/rec/users', {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          Accept: 'application/json',
          Authorization: process.env.AUTH_KEY
        }
      })
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            if (this._isMounted) {
              const usernames = ['Non-brother'].concat(json.users.map(mapToUsername)
                .sort(byAlpha));
              this.setState({ /* users: json.users, */ usernames });
            }
            resolve();
          } else {
            reject(json.msg);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  getMonthData() {
    return new Promise((resolve, reject) => {
      const { month } = this.state;
      fetch(`/api/rec/monthData/?month=${month}`)
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            if (this._isMounted) {
              this.setState({ data: json.data });
            }
            resolve(true);
          } else {
            console.error(json.msg);
            reject(json.msg);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  dateCellRender(value) {
    const { data } = this.state;
    let listData = [];
    if (data) {
      const byTime = (a, b) => {
        const aDate = new Date(a.date);
        const bDate = new Date(b.date);
        if (aDate.getTime() > bDate.getTime()) {
          return 1;
        }
        return -1;
      };
      listData = Array.from(data[value.toDate().toDateString()] || [])
        .sort(byTime);
      if (listData) {
        for (let i = 0; i < listData.length; i += 1) {
          const item = listData[i];
          const date = moment(new Date(item.date));
          const formattedTime = `${date.format('h:mm a')}`;
          const formattedDate = `${formattedTime} on ${date.format('MMMM Do')}`;
          const content = `${formattedTime} - ${item.players.length === 8 ? 'Full' : `Needs ${8 - item.players.length}`}`;
          const okText = item.inGame ? 'Drop' : 'Join';
          const gameClick = e => {
            e.stopPropagation();
            this.setState({
              game: formattedDate,
              players: item.players,
              alts: item.alts,
              createModal: false,
              visible: true,
              okText,
              selectedDay: value.toDate().toDateString(),
              selectedGame: item.date,
              showDelete: item.isCreator
            });
          };
          listData[i] = (
            <li key={item.date}>
              <Button
                icon="team"
                className={styles.game}
                onClick={gameClick}
              >
                {content}
              </Button>
            </li>
          );
        }
      }
    }

    return (
      <ul>
        {listData}
      </ul>
    );
  }

  updatePlayer(value, id) {
    if (id === '1' && !value) {
      this.setState({ alert: true, alertText: 'At least one player is required', alertClosable: false });
    } else {
      this.setState({ alert: false });
    }

    const idx = Number.parseInt(id, 10);

    const { createPlayers } = this.state;
    createPlayers[idx - 1] = value;
    const flattenedPlayers = createPlayers.filter(player => player && player !== '');

    this.setState({ createPlayers: flattenedPlayers, initial: false });
  }

  updateAlt(e) {
    const { value, id } = e.target;
    const idx = Number.parseInt(id, 10);
    const { createAlts } = this.state;
    createAlts[idx - 1] = value;
    this.setState({ createAlts });
  }

  updateTime(time) {
    const { selectedValue } = this.state;
    const datetime = new Date(time);
    const timeHours = datetime.getHours();
    const timeMinutes = datetime.getMinutes();
    const combinedDate = selectedValue.toDate();
    combinedDate.setHours(timeHours);
    combinedDate.setMinutes(timeMinutes);
    const smallDateString = combinedDate
      .toString()
      .substring(0, combinedDate.toString()
        .lastIndexOf(':'));

    this.setState({ createTime: smallDateString });
  }

  createGame(val) {
    let { selectedValue } = this.state;
    if (val._isAMomentObject) {
      selectedValue = val;
    }
    const formattedDate = `${selectedValue.format('MMMM Do')}`;
    this.setState({
      createModal: true,
      okText: 'Create',
      game: formattedDate
    });
    this.showModal();
  }

  showModal() {
    this.setState({
      visible: true
    });
  }

  findGameWithDate(date, day) {
    const { data } = this.state;
    const dayGames = data[day];
    for (let i = 0; i < dayGames.length; i += 1) {
      if (dayGames[i].date === date) {
        return dayGames[i];
      }
    }
    return {};
  }

  deleteGame() {
    const { selectedDay, selectedGame } = this.state;
    const game = this.findGameWithDate(selectedGame, selectedDay);
    fetch('/api/rec/deleteGame', {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        gameId: game._id
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            this.getMonthData();
            this.setState({
              visible: false,
              confirmLoading: false,
              initial: true,
              createPlayers: [],
              createAlts: [],
              createTime: moment(Date.now())
            });
          }
        } else {
          console.error(json.msg);
          this.setState({
            alert: true,
            alertText: json.msg,
            alertType: 'error',
            alertClosable: true,
            confirmLoading: false
          });
        }
      })
      .catch(err => {
        console.error(err);
        this.setState({
          alert: true,
          alertText: err.message,
          alertType: 'error',
          alertClosable: true,
          confirmLoading: false
        });
      });
  }

  handleCreateOk() {
    const {
      createPlayers, createAlts, createTime
    } = this.state;
    this.setState({
      confirmLoading: true,
    });

    fetch('/api/rec/createGame', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        date: createTime,
        players: createPlayers,
        alts: createAlts
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            this.getMonthData();
            this.setState({
              visible: false,
              confirmLoading: false,
              initial: true,
              createPlayers: [],
              createAlts: [],
              createTime: moment(Date.now())
            });
          }
        } else {
          console.error(json.msg);
          if (this._isMounted) {
            this.setState({
              alert: true,
              alertText: json.msg,
              alertType: 'error',
              alertClosable: true,
              confirmLoading: false
            });
          }
        }
      })
      .catch(err => {
        console.error(err);
        if (this._isMounted) {
          this.setState({
            alert: true,
            alertText: err.message,
            alertType: 'error',
            alertClosable: true,
            confirmLoading: false
          });
        }
      });
  }

  handleInfoOk() {
    this.setState({
      confirmLoading: true,
    });

    const { selectedDay, selectedGame } = this.state;
    let game = this.findGameWithDate(selectedGame, selectedDay);
    const action = game.inGame ? 'removePlayer' : 'addPlayer';
    fetch(`/api/rec/${action}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        gameId: game._id
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const msg = game.inGame ? 'Dropped' : 'Joined';
            this.setState({
              alert: true,
              alertText: `${msg} successfully`,
              alertType: 'success',
              alertClosable: true,
              confirmLoading: false,
            });
            this.getMonthData()
              .then(() => {
                if (this._isMounted) {
                  game = this.findGameWithDate(selectedGame, selectedDay);
                  const okText = game.inGame ? 'Drop' : 'Join';
                  if (game.players.length === 0) {
                    this.deleteGame();
                  }
                  this.setState({ players: game.players, alts: game.alts, okText });
                }
              })
              .catch(err => {
                console.error(err);
              });
          }
        } else {
          console.error(json.msg);
          if (this._isMounted) {
            this.setState({
              alert: true,
              alertText: json.msg,
              alertType: 'error',
              alertClosable: true,
              confirmLoading: false
            });
          }
        }
      })
      .catch(err => {
        console.error(err);
        if (this._isMounted) {
          this.setState({
            alert: true,
            alertText: err.message,
            alertType: 'error',
            alertClosable: true,
            confirmLoading: false
          });
        }
      });
  }

  handleCancel() {
    this.setState({
      visible: false,
      alert: false,
      initial: true,
    });
  }

  render() {
    const {
      visible,
      confirmLoading,
      players,
      alts,
      value,
      game,
      createModal,
      okText,
      alert,
      initial,
      alertText,
      alertClosable,
      alertType,
      showDelete,
      usernames,
      createPlayers,
      selectedGame
    } = this.state;
    let validUsername = true;
    const seen = {};
    for (let i = 0; i < createPlayers.length; i += 1) {
      if (createPlayers[i]) {
        if (createPlayers[i] !== 'Non-brother' && Object.prototype.hasOwnProperty.call(seen, createPlayers[i])) {
          validUsername = false;
        } else {
          seen[createPlayers[i]] = true;
        }
        if (!usernames.includes(createPlayers[i])) {
          validUsername = false;
        }
      }
    }
    const disabled = (createModal && (initial || alert || !validUsername))
      || (!createModal && (players.length >= 8 || (new Date(selectedGame)).getTime() < (new Date()).getTime()));

    const filteredUsernames = usernames.filter(user => user === 'Non-brother' || !createPlayers.includes(user.toUpperCase()));
    const playerInputs = [];
    const createInput = i => {
      return (
        <AutoComplete
          filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
          onChange={val => this.updatePlayer(val, i)}
          style={{ margin: '5px 0' }}
          key={i}
          dataSource={filteredUsernames}
        >
          <Input
            id={i.toString()}
            key={i}
            addonBefore={i.toString()}
            placeholder="Player"
          />
        </AutoComplete>
      );
    };
    for (let i = 1; i <= 8; i += 1) {
      playerInputs.push(createInput(i));
    }

    return (
      <div style={{ textAlign: 'center' }}>
        <Calendar
          value={value}
          onSelect={this.onSelect}
          onPanelChange={this.onPanelChange}
          onChange={this.onChange}
          disabledDate={Scheduling.disabledDate}
          dateCellRender={this.dateCellRender}
        />
        <Button type="primary" icon="plus" onClick={this.createGame}>
          Create Game
        </Button>
        {createModal ? (
          <CreateGameModal
            alert={alert}
            alertText={alertText}
            alertType={alertType}
            alertClosable={alertClosable}
            updateAlt={this.updateAlt}
            updateTime={this.updateTime}
            playerInputs={playerInputs}
            game={game}
            visible={visible}
            handleCreateOk={this.handleCreateOk}
            confirmLoading={confirmLoading}
            handleCancel={this.handleCancel}
            okText={okText}
            disabled={disabled}
          />
        ) : (
          <GameInfoModal
            alert={alert}
            alts={alts}
            players={players}
            alertText={alertText}
            alertType={alertType}
            alertClosable={alertClosable}
            showDelete={showDelete}
            game={game}
            visible={visible}
            handleInfoOk={this.handleInfoOk}
            confirmLoading={confirmLoading}
            handleCancel={this.handleCancel}
            okText={okText}
            disabled={disabled}
            deleteGame={this.deleteGame}
          />)
          }
      </div>
    );
  }
}

export default Scheduling;
