import React, { Component } from 'react';
import {
  Alert, Col, Input, Row, TimePicker, Modal
} from 'antd';

class CreateGameModal extends Component { // eslint-disable-line
  constructor(props) { // eslint-disable-line
    super(props);
  }

  render() {
    const {
      alert, alertText, alertType, alertClosable, updateAlt, updateTime, playerInputs, game, visible, handleCreateOk, confirmLoading, handleCancel, okText, disabled
    } = this.props;

    return (
      <Modal
        destroyOnClose
        title={`Create game for ${game}`}
        visible={visible}
        onOk={handleCreateOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText={okText}
        okButtonProps={{ disabled }}
        cancelButtonProps={{ type: 'danger' }}
      >
        <div style={{ textAlign: 'center' }}>
          <Row>
            <TimePicker onChange={updateTime} format="h:mm a" defaultValue={null} use12Hours minuteStep={5} />
          </Row>
          <Row style={{ padding: '25px 0' }}>
            <Col span={10}>
              <h3>
                Players
              </h3>
              {playerInputs}
            </Col>
            <Col span={10} offset={4}>
              <h3>
                Alternates
              </h3>
              <Input style={{ margin: '5px 0' }} id="1" onChange={updateAlt} addonBefore="1" placeholder="Alternate" />
              <Input style={{ margin: '5px 0' }} id="2" onChange={updateAlt} addonBefore="2" placeholder="Alternate" />
              <Input style={{ margin: '5px 0' }} id="3" onChange={updateAlt} addonBefore="3" placeholder="Alternate" />
            </Col>
          </Row>
          <Row>
            {alert ? (
              <Alert message={alertText} type={alertType} closable={alertClosable} showIcon />
            ) : ''
            }
          </Row>
        </div>
      </Modal>
    );
  }
}

export default CreateGameModal;
