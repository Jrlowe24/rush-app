import React, { Component } from 'react';
import {
  Alert, Button, Col, List, Row, Modal
} from 'antd';

class GameInfoModal extends Component { // eslint-disable-line
  constructor(props) { // eslint-disable-line
    super(props);
  }

  render() {
    const {
      alert, alts, players, alertText, alertType, alertClosable, showDelete, game, visible, handleInfoOk, confirmLoading, handleCancel, okText, disabled, deleteGame
    } = this.props;
    return (
      <Modal
        destroyOnClose
        title={`Game at ${game}`}
        visible={visible}
        onOk={handleInfoOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText={okText}
        okButtonProps={{ disabled }}
      >
        <div>
          <Row>
            <Col span={10}>
              <h3>
                In
              </h3>
              <List
                size="small"
                bordered
                dataSource={players}
                renderItem={item => (
                  <List.Item>
                    {item}
                  </List.Item>
                )}
              />
            </Col>
            <Col span={10} offset={4}>
              <h3>
                Alt
              </h3>
              <List
                size="small"
                bordered
                dataSource={alts}
                renderItem={item => (
                  <List.Item>
                    {item}
                  </List.Item>
                )}
              />
            </Col>
          </Row>
          <Row style={{ marginTop: '2%' }}>
            {showDelete ? (
              <Button icon="minus-circle" onClick={deleteGame} type="danger">
                Delete
              </Button>
            ) : ''}
          </Row>
          <Row>
            {alert ? (
              <Alert message={alertText} type={alertType} closable={alertClosable} showIcon />
            ) : ''}
          </Row>
        </div>
      </Modal>
    );
  }
}

export default GameInfoModal;
