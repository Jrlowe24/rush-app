import React, { Component } from 'react';
import { Select, Row } from 'antd';
import PlayerTable from './components/PlayerTable';
import styles from './Players.css';

const { Option } = Select;

class Players extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      players: [],
      seasons: [],
      season: 0
    };

    this.fetchSeason = this.fetchSeason.bind(this);
    this.fetchSeasons = this.fetchSeasons.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchSeasons();
  }

  componentDidUpdate(prevProps, prevState) {
    const { season } = this.state;
    if (prevState.season !== season) this.fetchSeason();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchSeason() {
    const { season } = this.state;
    fetch(`/api/league/players?season=${season}`)
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const players = json.players.filter(player => player.statistics.length !== 0);
            this.setState({ players });
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchSeasons() {
    fetch('/api/league/seasons')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const season = json.seasons[json.seasons.length - 1];
            this.setState({ seasons: json.seasons, season });
            this.fetchSeason();
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const { players, seasons, season } = this.state;
    const options = [];
    const createOptions = i => {
      return (
        <Option value={seasons[i]} key={i}>
          {seasons[i]}
        </Option>
      );
    };
    for (let i = 0; i < seasons.length; i += 1) {
      options.push(createOptions(i));
    }

    return (
      <div style={{ height: '100%', textAlign: 'center' }}>
        <Row style={{ margin: '0 2%' }}>
          <span>
            Season:&nbsp;
          </span>
          <Select className={styles.select} value={season} onChange={value => this.setState({ season: value })}>
            {options}
          </Select>
        </Row>
        <Row style={{ margin: '0 2%' }}>
          <PlayerTable players={players} />
        </Row>
      </div>
    );
  }
}

export default Players;
