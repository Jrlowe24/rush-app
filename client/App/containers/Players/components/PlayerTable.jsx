import React, { Component } from 'react';
import { Popover, Table, Button } from 'antd';

class PlayerTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sorter: { order: false },
      scrollY: window.innerHeight * 0.93
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { draft } = this.props;
    const pos = document.getElementsByClassName('ant-table-thead')[0].getBoundingClientRect().bottom;
    let scrollY = window.innerHeight - pos;
    if (!draft) scrollY -= 50;
    this.setState({ scrollY });

    Array.from(document.getElementsByClassName('anticon-caret-up'))
      .forEach(icon => icon.style.color = '#fff'); // eslint-disable-line
    Array.from(document.getElementsByClassName('anticon-caret-down'))
      .forEach(icon => icon.style.color = '#fff'); // eslint-disable-line
  }

  onHeaderCell = col => {
    const { sorter } = this.state;
    const orders = ['ascend', 'descend', false];
    const cols = ['name', 'average', 'atBats', 'hits', 'onBase', 'homeRuns', 'runsBattedIn', 'runs', 'errors', 'strikeouts'];
    let ord = (orders.indexOf(sorter.order) + 1) % 3;
    let changeKey = false;
    if (col.key !== sorter.columnKey) {
      ord = 0;
      changeKey = true;
    }

    return {
      style: {
        // backgroundColor: '#001529',
        // color: '#fff',
        cursor: 'pointer',
      },
      onClick: () => {
        if (orders[ord] === 'ascend') this.highlightSortArrow(col, 'up', changeKey);
        else if (orders[ord] === 'descend') this.highlightSortArrow(col, 'down', changeKey);
        else this.highlightSortArrow(col, false, changeKey);
        this.setState({ sorter: { order: orders[ord], columnKey: col.key } });
      },
      onMouseEnter: () => {
        document.getElementsByClassName('ant-table-column-has-filters')[cols.indexOf(col.key)].style.backgroundColor = '#001529';
      },
      onMouseLeave: () => {
        document.getElementsByClassName('ant-table-column-has-filters')[cols.indexOf(col.key)].style.backgroundColor = '#444444';
      }
    };
  };

  highlightSortArrow = (col, dir, changeKey) => {
    const cols = ['name', 'average', 'atBats', 'hits', 'onBase', 'homeRuns', 'runsBattedIn', 'runs', 'errors', 'strikeouts'];
    if (changeKey || dir !== 'up') {
      Array.from(document.getElementsByClassName('anticon-caret-up')).forEach(icon => icon.style.color = '#ffffff'); // eslint-disable-line
      Array.from(document.getElementsByClassName('anticon-caret-down')).forEach(icon => icon.style.color = '#ffffff'); // eslint-disable-line
    }
    if (dir) document.getElementsByClassName(`anticon-caret-${dir}`)[cols.indexOf(col.key)].style.color = '#faad14';
  };

  handleChange(pagination, filters, sorter) {
    this.setState({ sorter });
  }

  render() {
    const {
      players, style, draft, onDraft, canDraft
    } = this.props;
    let { sorter } = this.state;
    const { scrollY } = this.state;
    sorter = sorter || {};

    const createSorter = stat => {
      return (a, b) => {
        const fallback = sorter.order === 'ascend' ? -1 : 1000;
        const aStats = a.statistics[0] || {};
        const bStats = b.statistics[0] || {};
        const aStat = aStats[stat] || fallback;
        const bStat = bStats[stat] || fallback;
        return bStat - aStat;
      };
    };

    let columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name.localeCompare(b.username, 'en', { sensitivity: 'base' }),
      sortOrder: sorter.columnKey === 'name' && sorter.order,
      width: 150
    }, {
      title: 'Average',
      dataIndex: 'statistics[0].average',
      key: 'average',
      sorter: createSorter('average'),
      sortOrder: sorter.columnKey === 'average' && sorter.order,
      width: 100
    }, {
      title: 'At Bats',
      dataIndex: 'statistics[0].atBats',
      key: 'atBats',
      sorter: createSorter('atBats'),
      sortOrder: sorter.columnKey === 'atBats' && sorter.order,
      width: 100
    }, {
      title: 'Hits',
      dataIndex: 'statistics[0].hits',
      key: 'hits',
      sorter: createSorter('hits'),
      sortOrder: sorter.columnKey === 'hits' && sorter.order,
      width: 100
    }, {
      title: 'On Base',
      dataIndex: 'statistics[0].onBase',
      key: 'onBase',
      sorter: createSorter('onBase'),
      sortOrder: sorter.columnKey === 'onBase' && sorter.order,
      width: 100
    }, {
      title: 'Home Runs',
      dataIndex: 'statistics[0].homeRuns',
      key: 'homeRuns',
      sorter: createSorter('homeRuns'),
      sortOrder: sorter.columnKey === 'homeRuns' && sorter.order,
      width: 100
    }, {
      title: 'Runs Batted In',
      dataIndex: 'statistics[0].runsBattedIn',
      key: 'runsBattedIn',
      sorter: createSorter('runsBattedIn'),
      sortOrder: sorter.columnKey === 'runsBattedIn' && sorter.order,
      width: 100
    }, {
      title: 'Runs',
      dataIndex: 'statistics[0].runs',
      key: 'runs',
      sorter: createSorter('runs'),
      sortOrder: sorter.columnKey === 'runs' && sorter.order,
      width: 100
    }, {
      title: 'Errors',
      dataIndex: 'statistics[0].numErrors',
      key: 'errors',
      sorter: createSorter('numErrors'),
      sortOrder: sorter.columnKey === 'errors' && sorter.order,
      width: 100
    }, {
      title: 'Strikeouts',
      dataIndex: 'statistics[0].strikeouts',
      key: 'strikeouts',
      sorter: createSorter('strikeouts'),
      sortOrder: sorter.columnKey === 'strikeouts' && sorter.order,
      width: 100
    }];

    const render = (text, record) => {
      const content = (
        <Button type="primary" onClick={() => onDraft(record)} disabled={!canDraft}>
          Draft
        </Button>
      );
      return (
        <Popover content={content}>
          {text}
        </Popover>
      );
    };

    columns = columns.map(col => Object.assign(col, { onHeaderCell: this.onHeaderCell }));
    if (draft) columns[0].render = render;

    const newStyle = style || {};
    if (!draft) newStyle.marginBottom = 50;

    return (
      <Table
        style={newStyle}
        columns={columns}
        dataSource={players}
        onChange={this.handleChange}
        rowKey="_id"
        pagination={false}
        scroll={{ x: 1000, y: scrollY }}
      />
    );
  }
}

export default PlayerTable;
