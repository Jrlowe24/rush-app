import React, { Component } from 'react';
import { Card, Row, Tooltip } from 'antd';
import PlayerTable from '../Players/components/PlayerTable';
import styles from './Draft.css';

class Draft extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      player: {},
      players: [],
      captains: [],
      drafteeMap: {},
      draftees: [],
      draftPick: 0,
      season: 0,
      draftStatus: false,
      seasonStatus: false,
      isMobile: false
    };

    this.fetchDraftees = this.fetchDraftees.bind(this);
    this.fetchPlayers = this.fetchPlayers.bind(this);
    this.fetchSeasons = this.fetchSeasons.bind(this);
    this.onDraft = this.onDraft.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    fetch('/api/league/info')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            this.setState({ draftStatus: json.meta.draft, seasonStatus: json.meta.seasonInProgress });
            if (json.meta.draft) {
              this.draftUpdate = setInterval(this.fetchDraftees, 2000);
              this.fetchDraftees();
            }
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
    fetch('/api/league/player')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) this.setState({ player: json.player });
        } else console.error(json.msg);
      })
      .catch(console.error);
    fetch('/api/mobile')
      .then(res => res.json())
      .then(json => {
        if (json.success && this._isMounted) this.setState({ isMobile: json.isMobile });
      })
      .catch(console.error);
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(this.draftUpdate);
    clearTimeout(this.endDraftTime);
  }

  onDraft(player) {
    const { players } = this.state;
    this.setState({ players: players.filter(pl => pl.name !== player.name) });
    fetch('/api/league/draftPlayer', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: player._id
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) this.fetchDraftees();
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  endDraft = () => {
    fetch('/api/league/endDraft')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.endDraftTime = setTimeout(() => this.setState({ draftStatus: json.meta.draft }), 60000);
          clearInterval(this.draftUpdate);
        } else console.error(json.msg);
      })
      .catch(console.error);
  };

  fetchDraftees() {
    fetch('/api/league/draftees')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const draftees = [];
            Object.values(json.draftees)
              .forEach(players => players.forEach(player => draftees.push(player.name)));
            this.setState({ drafteeMap: json.draftees, draftees });
            this.fetchDraftPick();
            this.fetchSeasons();
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchDraftPick() {
    fetch('/api/league/draftPick')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) this.setState({ draftPick: json.draftPick });
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchPlayers() {
    const { season, draftees } = this.state;
    fetch('/api/league/players')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            let players = json.players.map(player => {
              const stats = player.statistics.filter(stat => stat.season === `Spring ${season}`);
              return Object.assign(player, { statistics: stats });
            });
            const byDraftPick = (a, b) => a.draftPick - b.draftPick;
            const captains = players.filter(player => player.isCaptain)
              .sort(byDraftPick);
            players = players.filter(player => !player.isCaptain)
              .filter(player => player.inDraft)
              .filter(player => !draftees.includes(player.name));
            if (players.length === 0) this.endDraft();
            this.setState({ players, captains });
            Array.from(document.getElementsByClassName('ant-card-head-title'))
              .forEach(el => el.style.color = '#fff'); // eslint-disable-line
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchSeasons() {
    fetch('/api/league/seasons')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const season = json.seasons[json.seasons.length - 1];
            this.setState({ season });
            this.fetchPlayers();
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const {
      player, players, captains, drafteeMap, draftPick, draftStatus, seasonStatus, isMobile
    } = this.state;
    const captainCards = [];
    const createCaptainCards = i => {
      const style = {
        width: `${isMobile ? 100 / Math.ceil(captains.length / 3) : 100 / Math.ceil(captains.length / 2)}%`,
        marginBottom: '5%',
        backgroundColor: '#001529',
        color: '#fff',
        maxHeight: isMobile ? '25%' : 'fit-content',
        overflowY: isMobile ? 'scroll' : 'auto',
        fontSize: '60%'
      };
      if (captains[i].draftPick === draftPick && !this.endDraftTime) style.backgroundColor = '#e3b210';
      const captainsDraftees = drafteeMap[captains[i]._id.toString()] || [];
      return (
        <Tooltip title={captains[i].name} trigger="click" key={i}>
          <Card style={style} title={captains[i].name}>
            {captainsDraftees.map(
              player => <span key={player._id}>{player.name}<br /></span> /* eslint-disable-line */
            )}
          </Card>
        </Tooltip>
      );
    };

    for (let i = 0; i < captains.length; i += 1) {
      captainCards.push(createCaptainCards(i));
    }
    if (isMobile) Array.from(document.getElementsByClassName('ant-card-head-title')).forEach(el => el.style.fontSize = '110%'); // eslint-disable-line

    const canDraft = players.length > 0 && player.isCaptain && player.draftPick === draftPick;
    let message = 'Draft not currently in progress';
    if (seasonStatus) message = 'Season currently in progress';

    return (
      <div className={styles.container}>
        {draftStatus ? (
          <div style={{ height: '100%', width: '100%' }}>
            <div className={styles.captains}>
              <Row type="flex" justify="space-between">
                {captainCards}
              </Row>
            </div>
            <div className={styles.players}>
              <PlayerTable players={players} draft onDraft={this.onDraft} canDraft={canDraft} />
            </div>
          </div>
        ) : (
          <Row style={{ height: '100%' }} type="flex" justify="center" align="middle">
            <span style={{ fontSize: '26pt' }}>
              {message}
            </span>
          </Row>
        )}
      </div>
    );
  }
}

export default Draft;
