import React, { Component } from 'react';
import { Card } from 'antd';

const { Grid } = Card;

const gridStyle = {
  width: '50%',
  textAlign: 'center',
};

class SchedulingCard extends Component { // eslint-disable-line
  constructor(props) { // eslint-disable-line
    super(props);
  }

  render() {
    const { isMobile } = this.props;
    return (
      <Card style={{ width: isMobile ? '90%' : '40%', margin: isMobile ? '8% auto' : 0 }} title="Scheduling" hoverable>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
        <Grid style={gridStyle}>
          Content
        </Grid>
      </Card>
    );
  }
}

export default SchedulingCard;
