import React, { Component } from 'react';
import {
  Card, Row, Select, Tag, Col
} from 'antd';
import styles from '../../Players/Players.css';

const { Option } = Select;

class LeaderboardCard extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      stat: 'Average',
      season: 0,
      players: [],
      leaders: new Array(8)
    };

    this.fetchSeason = this.fetchSeason.bind(this);
    this.fetchSeasons = this.fetchSeasons.bind(this);
    this.getLeaders = this.getLeaders.bind(this);
  }

  componentDidMount() {
    this.fetchSeasons();
    this.getLeaders('average');
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getLeaders(stat) {
    const { players } = this.state;
    const byStatReverse = (a, b) => b.statistics[0][stat] - a.statistics[0][stat];
    const leaders = players.sort(byStatReverse)
      .slice(0, 8);
    this.setState({ leaders, stat });
  }

  fetchSeason() {
    const { season } = this.state;
    fetch(`/api/league/players?season=${season}`)
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            const players = json.players.filter(player => player.statistics.length !== 0);
            this.setState({ players });
            this.getLeaders('average');
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchSeasons() {
    fetch('/api/league/seasons')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          const season = json.seasons[json.seasons.length - 1];
          if (this._isMounted) this.setState({ season });
          this.fetchSeason();
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const { isMobile } = this.props;
    const { stat, leaders } = this.state;
    const stats = ['average', 'atBats', 'hits', 'onBase', 'homeRuns', 'runsBattedIn', 'runs', 'numErrors', 'strikeouts'];
    const options = [];
    const grids = [];
    const capitalizeAndSpace = str => {
      let cap = `${str[0].toUpperCase()}${str.slice(1)}`;
      cap = cap.split(/([A-Z][^A-Z\s]+)/)
        .filter(word => word !== '')
        .join(' ');
      if (cap === 'Num Errors') cap = 'Errors';
      return cap;
    };
    const createOptions = i => {
      return (
        <Option value={stats[i]} key={i}>
          {capitalizeAndSpace(stats[i])}
        </Option>
      );
    };
    for (let i = 0; i < stats.length; i += 1) {
      options.push(createOptions(i));
    }

    const createDivs = i => {
      if (leaders[i]) {
        return (
          <div style={{ marginBottom: 10 }} key={i}>
            <Tag color="blue">
              {`${i + 1}. `}
            </Tag>
            <span>
              {`${leaders[i].name}: ${leaders[i].statistics[0][stat]}`}
            </span>
          </div>
        );
      }
      return (
        <p key={i}>
          Loading
        </p>
      );
    };
    for (let i = 0; i < leaders.length; i += 1) {
      grids.push(createDivs(i));
    }

    return (
      <Card style={{ margin: isMobile ? '0 auto' : 0, marginBottom: isMobile ? '8%' : 0, width: isMobile ? '90%' : '40%' }} title="Leaderboard" hoverable>
        <Row style={{ width: '100%', paddingBottom: '5%' }} type="flex" justify="center" align="middle">
          <span>
              Statistic:&nbsp;
          </span>
          <Select className={styles.select} style={{ width: 150 }} value={stat} onChange={this.getLeaders}>
            {options}
          </Select>
        </Row>
        <Row type="flex" justify="space-between">
          <Col>
            {grids.slice(0, leaders.length / 2)}
          </Col>
          <Col>
            {grids.slice(leaders.length / 2)}
          </Col>
        </Row>
      </Card>
    );
  }
}

export default LeaderboardCard;
