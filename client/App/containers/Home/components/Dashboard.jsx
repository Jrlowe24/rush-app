import React, { Component } from 'react';
import { Row } from 'antd';
import SchedulingCard from './SchedulingCard';
import LeaderboardCard from './LeaderboardCard';
import BCPanel from './BCPanel';
import DrafteesPanel from './DrafteesPanel';

class Dashboard extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      declared: []
    };

    this.fetchDeclared = this.fetchDeclared.bind(this);
  }

  componentDidMount() {
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchDeclared() {
    fetch('/api/league/declared')
      .then(res => res.json())
      .then(json => {
        if (json.success) this.setState({ declared: json.declared });
        else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const { declared } = this.state;
    return (
      <div>
        <Row style={{ margin: '2%' }} type="flex" justify="space-around">
          <SchedulingCard />
          <LeaderboardCard />
        </Row>
        <Row style={{ margin: '2%' }} type="flex" justify="space-around">
          <BCPanel refresh={this.fetchDeclared} />
          <DrafteesPanel declared={declared} />
        </Row>
      </div>
    );
  }
}

export default Dashboard;
