import React, { Component } from 'react';
import {
  Card, Button, List, Row, Modal, Input, AutoComplete
} from 'antd';

const { TextArea } = Input;

class BCPanel extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      meta: {},
      captains: [],
      players: [],
      addCap: '',
      visible: false,
      captain: true,
      draftees: ''
    };

    this.startDraft = this.startDraft.bind(this);
    this.fetchCaptains = this.fetchCaptains.bind(this);
    this.fetchPlayers = this.fetchPlayers.bind(this);
    this.addCaptain = this.addCaptain.bind(this);
    this.addDraftees = this.addDraftees.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    fetch('/api/league/info')
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) this.setState({ meta: json.meta });
        } else console.error(json.msg);
      })
      .catch(console.error);
    this.fetchCaptains();
    this.fetchPlayers();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchCaptains() {
    const byDraftPick = (a, b) => a.draftPick - b.draftPick;
    fetch('/api/league/players')
      .then(res => res.json())
      .then(json => {
        const captains = json.players.filter(player => player.isCaptain)
          .sort(byDraftPick);
        if (json.success) {
          if (this._isMounted) this.setState({ captains });
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  fetchPlayers() {
    fetch('/api/league/players')
      .then(res => res.json())
      .then(json => {
        const byName = (a, b) => a.name.localeCompare(b.username, 'en', { sensitivity: 'base' });
        if (json.success) {
          if (this._isMounted) this.setState({ players: json.players.sort(byName) });
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  addCaptain() {
    const { addCap, players } = this.state;
    const id = players.filter(player => player.name === addCap)[0]._id;
    fetch('/api/league/addCaptain', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            this.fetchCaptains();
            this.fetchPlayers();
            this.setState({ visible: false });
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  addDraftees() {
    const { refresh } = this.props;
    const { draftees } = this.state;
    fetch('/api/league/draftPlayers', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        usernames: draftees.split('\n')
          .map(u => u.toLowerCase())
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.fetchCaptains();
          this.fetchPlayers();
          refresh();
          this.setState({ visible: false });
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  removeCaptain(player) {
    fetch('/api/league/removeCaptain', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: player._id
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) {
            this.fetchCaptains();
            this.fetchPlayers();
          }
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  startDraft() {
    fetch('/api/league/draft', {
      method: 'POST',
      headers: {
        Accept: 'application/json'
      }
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          if (this._isMounted) this.setState({ meta: json.meta });
        } else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const { isMobile } = this.props;
    const {
      meta, captains, players, visible, addCap, captain
    } = this.state;
    return (
      <Card
        style={{
          width: isMobile ? '90%' : '40%', textAlign: 'center', margin: isMobile ? '0 auto' : 0, marginBottom: isMobile ? '8%' : 0
        }}
        title="Beercan Admin Panel"
        hoverable
      >
        <Row type="flex" justify="space-around" align="middle">
          <span>
            Current season:&nbsp;
            {meta.season}
          </span>
          <Button type="primary" disabled={meta.draft || meta.seasonInProgress} onClick={this.startDraft}>
            Start Draft
          </Button>
        </Row>
        <p>
          Team Captains&nbsp;(
          {captains.length}
          ) - Ordered by draft pick
        </p>
        <List
          style={{
            maxHeight: 200, overflowY: 'scroll', backgroundColor: '#001529', color: '#fff'
          }}
          bordered
          dataSource={captains}
          renderItem={item => (
            <List.Item
              actions={[
                <Button type="danger" onClick={() => this.removeCaptain(item)} disabled={meta.draft || meta.seasonInProgress}>
                  Remove
                </Button>
              ]}
            >
              &nbsp;&nbsp;&nbsp;
              {item.name}
            </List.Item>
          )}
        />
        <br />
        <Button type="primary" onClick={() => this.setState({ visible: true, captain: true })} disabled={meta.draft || meta.seasonInProgress}>
          Add Captain
        </Button>
        &nbsp;in order of draft pick
        <br />
        <br />
        <Button type="primary" onClick={() => this.setState({ visible: true, captain: false })} disabled={meta.draft || meta.seasonInProgress}>
          Add Draftees
        </Button>
        <Modal
          style={{ textAlign: 'center' }}
          okText="Add"
          okType="primary"
          okButtonProps={{
            disabled: captain && !players.map(player => player.name)
              .includes(addCap)
          }}
          onOk={captain ? this.addCaptain : this.addDraftees}
          visible={visible}
          onCancel={() => this.setState({ visible: false })}
          destroyOnClose
          maskClosable
        >
          {captain ? (
            <AutoComplete
              filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
              onChange={val => this.setState({ addCap: val })}
              style={{ margin: '5px 0' }}
              dataSource={players.filter(player => !player.isCaptain)
                .map(player => player.name)}
            >
              <Input
                addonBefore="Captain"
                placeholder="User"
              />
            </AutoComplete>
          ) : (
            <TextArea placeholder="Draftees' Initials on separate lines" onChange={e => this.setState({ draftees: e.target.value })} autosize={{ minRows: 15 }} />
          )}
        </Modal>
      </Card>
    );
  }
}

export default BCPanel;
