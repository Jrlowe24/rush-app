import React, { Component } from 'react';
import { List, Card } from 'antd';

class DrafteesPanel extends Component { // eslint-disable-line
  constructor(props) { // eslint-disable-line
    super(props);
  }

  render() {
    const { isMobile, declared } = this.props;
    return (
      <Card
        style={{
          width: isMobile ? '90%' : '40%', textAlign: 'center', margin: isMobile ? '0 auto' : 0, marginBottom: isMobile ? '8%' : 0
        }}
        title="Declared Draftees"
        hoverable
      >
        <p>
          Number declared:&nbsp;
          {declared.length}
        </p>
        <List
          style={{
            maxHeight: 200, overflowY: 'scroll', backgroundColor: '#001529', color: '#fff'
          }}
          bordered
          dataSource={declared}
          renderItem={item => (
            <List.Item>
              &nbsp;&nbsp;&nbsp;
              {item.name}
            </List.Item>
          )}
        />
      </Card>
    );
  }
}

export default DrafteesPanel;
