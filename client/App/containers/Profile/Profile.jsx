import React, { Component } from 'react';
import {
  Icon, Card, Row, Button, Table,
} from 'antd';
import styles from './Profile.css';

class Profile extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      player: {},
      teams: [],
      isMobile: false
    };

    this.getPlayer = this.getPlayer.bind(this);
    this.getTeams = this.getTeams.bind(this);
    this.onDraft = this.onDraft.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getPlayer();
    this.getTeams();
    fetch('/api/mobile')
      .then(res => res.json())
      .then(json => {
        if (json.success && this._isMounted) this.setState({ isMobile: json.isMobile });
      })
      .catch(console.error);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onDraft() {
    fetch('/api/league/player/draft')
      .then(res => res.json())
      .then(json => {
        if (json.success && this._isMounted) this.setState({ player: json.player });
        else console.error(json.msg);
      })
      .catch(console.error);
  }

  getPlayer() {
    fetch('/api/league/player')
      .then(res => res.json())
      .then(json => {
        if (json.success && this._isMounted) this.setState({ player: json.player });
        else console.error(json.msg);
      })
      .catch(console.error);
  }

  getTeams() {
    fetch('/api/league/player/teams')
      .then(res => res.json())
      .then(json => {
        if (json.success && this._isMounted) this.setState({ teams: json.teams });
        else console.error(json.msg);
      })
      .catch(console.error);
  }

  render() {
    const { player, teams, isMobile } = this.state;
    const columns = [{
      title: 'Team',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: 'Season',
      dataIndex: 'season',
      key: 'season'
    }];
    return (
      <div className={styles.container}>
        <Card className={styles.profile} style={{ width: isMobile ? '90%' : '40%' }} title="Profile" hoverable>
          <Row style={{ width: '100%' }} type="flex" justify="center">
            <div className={styles.avatar}>
              <Icon style={{ fontSize: 80, color: '#fff' }} type="user" />
            </div>
          </Row>
          <p style={{ fontSize: '26pt' }}>
            {player.name}
          </p>
          <p style={{ fontWeight: 'bold' }}>
            Teams
          </p>
          <Table
            dataSource={teams}
            columns={columns}
            rowKey="_id"
            expandRowByClick
            expandedRowRender={record => record.players.map(plyr => <p key={plyr._id}>{plyr.name}</p>) /* eslint-disable-line */}
            pagination={false}
          />
          <br />
          <br />
          <p>
            <Button type="primary" onClick={this.onDraft}>
              {player.inDraft ? 'Undeclare' : 'Declare'}
            </Button>
            &nbsp;for draft
          </p>
        </Card>
      </div>
    );
  }
}

export default Profile;
