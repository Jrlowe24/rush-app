import React, { Component } from 'react';
import { Menu } from 'antd';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

const { Item } = Menu;
const itemStyle = {
  backgroundColor: 'inherit', height: '100%', display: 'table', float: 'left'
};
const linkStyle = { lineHeight: '100%', display: 'table-cell', verticalAlign: 'middle' };

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      key: '1'
    };
  }

  componentDidMount() {
    const { location } = this.props;
    const map = {
      '/': '1',
      '/profile': '2',
      '/draft': '3',
      '/players': '4',
      '/scheduling': '5'
    };
    const key = map[location.pathname];
    this.setState({ key });
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (prevProps.location !== location) {
      const map = {
        '/': '1',
        '/profile': '2',
        '/draft': '3',
        '/players': '4',
        '/scheduling': '5'
      };
      const key = map[location.pathname];
      this.setState({ key }); // eslint-disable-line
      Array.from(document.getElementsByClassName('ant-select-arrow'))
        .forEach(el => {
          el.style.color = '#bbbbbb'; // eslint-disable-line
        });
    }
  }

  render() {
    const { key } = this.state;
    return (
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={[key]}
        selectedKeys={[key]}
        style={{ height: '100%', textAlign: 'center' }}
      >
        <Item style={itemStyle} key="1">
          <Link style={linkStyle} to="/">
            Home
          </Link>
        </Item>
        <Item style={itemStyle} key="2">
          <Link style={linkStyle} to="/profile">
            Profile
          </Link>
        </Item>
        <Item style={itemStyle} key="3">
          <Link style={linkStyle} to="/draft">
            Draft
          </Link>
        </Item>
        <Item style={itemStyle} key="4">
          <Link style={linkStyle} to="/players">
            Players
          </Link>
        </Item>
        <Item style={itemStyle} key="5">
          <Link style={linkStyle} to="/scheduling">
            Scheduling
          </Link>
        </Item>
        <Item style={{ ...itemStyle, float: 'right' }} key="6">
          <a style={linkStyle} href="/logout">
            Logout
          </a>
        </Item>
      </Menu>
    );
  }
}

export default withRouter(Navigation);
