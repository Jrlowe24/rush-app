import React from 'react';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';
import Navigation from './Navigation';
import styles from './Header.css';

const LayoutHeader = Layout.Header;

export default function Header() {
  return (
    <LayoutHeader className={styles.header}>
      <div className={styles.logo}>
        <Link to="/">
          <img src="https://cdn.pikapp.net/pikapp-common/CoA.png" className={styles.icon} alt="" />
        </Link>
      </div>
      <Navigation />
    </LayoutHeader>
  );
}
