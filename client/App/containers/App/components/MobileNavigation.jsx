import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, Icon } from 'antd';

const { Item } = Menu;

const itemStyle = {
  display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '10%'
};
const linkStyle = {
  color: 'rgb(227, 178, 16)'
};

class MobileNavigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      key: '1'
    };
  }

  componentDidMount() {
    const { location } = this.props;
    const map = {
      '/': '1',
      '/profile': '2',
      '/draft': '3',
      '/players': '4',
      '/scheduling': '5'
    };
    const key = map[location.pathname];
    this.setState({ key });
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (prevProps.location !== location) {
      const map = {
        '/': '1',
        '/profile': '2',
        '/draft': '3',
        '/players': '4',
        '/scheduling': '5'
      };
      const key = map[location.pathname];
      this.setState({ key }); // eslint-disable-line
    }
  }

  render() {
    const { collapsed } = this.props;
    const { key } = this.state;
    return (
      <Menu
        mode="inline"
        style={{
          height: '100%', backgroundColor: 'rgba(0, 21, 41, 0.7)', border: 'none', position: 'fixed', left: 0, width: collapsed ? '80px' : '200px'
        }}
        defaultSelectedKeys={[key]}
        selectedKeys={[key]}
        inlineCollapsed={collapsed}
      >
        <Item style={itemStyle} key="0">
          <Link style={linkStyle} to="/">
            <img src="https://cdn.pikapp.net/pikapp-common/CoA.png" style={{ width: '32px', height: '32px' }} alt="" />
          </Link>
        </Item>
        <Item style={itemStyle} key="1">
          <Link style={linkStyle} to="/">
            <Icon type="home" />
            {collapsed ? null : 'Home'}
          </Link>
        </Item>
        <Item style={itemStyle} key="2">
          <Link style={linkStyle} to="/profile">
            <Icon type="profile" />
            {collapsed ? null : 'Profile'}
          </Link>
        </Item>
        <Item style={itemStyle} key="3">
          <Link style={linkStyle} to="/draft">
            <Icon type="menu-fold" />
            {collapsed ? null : 'Draft'}
          </Link>
        </Item>
        <Item style={itemStyle} key="4">
          <Link style={linkStyle} to="/players">
            <Icon type="team" />
            {collapsed ? null : 'Players'}
          </Link>
        </Item>
        <Item style={itemStyle} key="5">
          <Link style={linkStyle} to="/scheduling">
            <Icon type="calendar" />
            {collapsed ? null : 'Scheduling'}
          </Link>
        </Item>
        <Item style={itemStyle} key="6">
          <a style={linkStyle} href="/logout">
            <Icon type="logout" />
            {collapsed ? null : 'Logout'}
          </a>
        </Item>
      </Menu>
    );
  }
}

export default withRouter(MobileNavigation);
