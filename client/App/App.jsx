import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import styles from './App.css';
import Header from './containers/App/components/Header';
import Home from './containers/Home/Home';
import Scheduling from './containers/Scheduling/Scheduling';
import Players from './containers/Players/Players';
import Draft from './containers/Draft/Draft';
import Profile from './containers/Profile/Profile';
import MobileNavigation from './containers/App/components/MobileNavigation';

const { Content, Sider } = Layout;

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMobile: false,
      collapsed: true
    };
  }

  componentDidMount() {
    fetch('/api/mobile')
      .then(res => res.json())
      .then(json => {
        if (json.success) this.setState({ isMobile: json.isMobile });
      })
      .catch(console.error);

    Array.from(document.getElementsByClassName('ant-select-arrow'))
      .forEach(el => {
        el.style.color = '#bbbbbb'; // eslint-disable-line
      });
  }

  render() {
    const { isMobile, collapsed } = this.state;
    return (
      <div className={styles.container}>
        <Router>
          <Layout style={{ height: '100vh' }}>
            {isMobile ? (
              <Sider
                style={{
                  height: '100%', position: 'fixed', left: 0, zIndex: 2, backgroundColor: 'rgba(0, 21, 41, 0.7)'
                }}
                collapsible
                collapsed={collapsed}
                onCollapse={c => this.setState({ collapsed: c })}
              >
                <MobileNavigation collapsed={collapsed} />
              </Sider>
            ) : <Header />}
            <Content style={{
              height: isMobile ? '100%' : '93%', overflowY: 'scroll', position: 'relative', zIndex: 1, width: isMobile ? '80%' : '100%', marginLeft: isMobile ? '20%' : 0
            }}
            >
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/draft" component={Draft} />
                <Route exact path="/players" component={Players} />
                <Route exact path="/scheduling" component={Scheduling} />
              </Switch>
            </Content>
          </Layout>
        </Router>
      </div>
    );
  }
}
