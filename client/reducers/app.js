// Import Actions

// Initial State
const initialState = {};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

/* Selectors */

// Export Reducer
export default appReducer;
