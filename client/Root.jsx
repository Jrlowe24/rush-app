/**
 * Root Component
 */
import React from 'react';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import App from './App/App';
// import DevTools from './App/containers/App/components/DevTools/DevTools';

// Base stylesheet
require('./Root.css');

export default function Root({ store }) {
  return (
    <Provider store={store}>
      <div style={{ height: '100%' }}>
        <Helmet>
          <title>
            Beercan
          </title>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="shortcut icon" href="https://cdn.pikapp.net/pikapp-common/CoA.png" type="image/png" />
        </Helmet>
        <App />
        {/* {process.env.NODE_ENV === 'development' && <DevTools />} */}
      </div>
    </Provider>
  );
}

Root.propTypes = {
  store: PropTypes.object.isRequired // eslint-disable-line
};
