/**
 * Entry Script
 */
const path = require('path');
const result = require('dotenv').config({ path: path.resolve(__dirname, '.env') });
if (result.error) {
  throw result.error;
}

if (process.env.NODE_ENV === 'development') {
  // Babel polyfill to convert ES6 code in runtime
  require('@babel/polyfill');
  require('@babel/register');
}

require('./server/server');
