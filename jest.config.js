module.exports = {
  coverageDirectory: "./__tests__/coverage",
  coverageThreshold: {
    global: { // Bare minimum
      statements: 50,
      branches: 50,
      functions: 50,
      lines: 50
    }
  },
  coverageReporters: [
    "json",
    "lcov",
    "text",
    "html"
  ],
  collectCoverageFrom: [
    "**/*.js",
    "!**/node_modules/**",
    "!**/vendor.js"
  ],
  testMatch: [
    "**/__tests__/**/*(*.)test.js"
  ],
  verbose: true
};
