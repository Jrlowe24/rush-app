const Express = require('express');
const path = require('path');
const flash = require('express-flash');
const session = require('express-session');
const logger = require('morgan');
require('./util/mongodb');
require('./util/importFromAuth');

// Initialize the Express App
const app = new Express();

// Set Development modes checks
const isDevMode = process.env.NODE_ENV === 'development' || false;
const PORT = process.env.PORT || 3000;

const RedisStore = require('connect-redis')(session);

app.use(session({
  cookie: {
    maxAge: 1209600,
    domain: process.env.NODE_ENV === 'production' ? '.pikapp.net' : 'localhost'
  },
  secret: process.env.SECRET,
  resave: true,
  saveUninitialized: false,
  store: new RedisStore({
    ttl: 3600
  })
}));

// Run Webpack dev server in development mode
if (isDevMode) {
  // Webpack Requirements
  const webpack = require('webpack'); //eslint-disable-line
  const config = require('../webpack.config.dev'); //eslint-disable-line
  const webpackDevMiddleware = require('webpack-dev-middleware'); //eslint-disable-line
  const webpackHotMiddleware = require('webpack-hot-middleware'); //eslint-disable-line
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    watchOptions: {
      poll: 500,
    },
  }));
  app.use(webpackHotMiddleware(compiler));
}

// Apply body Parser and server public assets and routes
// app.use(compression());
app.use(logger('dev'));
app.use(flash());
app.use(Express.json());
app.use(Express.urlencoded({ extended: false }));
app.use(Express.static(path.resolve(__dirname, '../public')));

require('./routes')(app);

// start app
app.listen(PORT, error => {
  if (!error) {
    console.log(`Beercan is running on port ${PORT}! Load http://localhost:${PORT} in your browser.`);
  }
});

module.exports = app;
