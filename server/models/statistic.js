const mongoose = require('mongoose');

const { Schema } = mongoose;

// Want it to fit the format (Fall, Spring, or Summer) **** (where * is a digit)
const validateSeason = val => /^(?:Fall|Spring|Summer) \d{4}$/.test(val);

const statisticSchema = new Schema({
  player: { type: Schema.Types.ObjectId, ref: 'Player', required: true },
  season: { type: String, /* required: true, */validate: { validator: validateSeason } },
  game: { type: Schema.Types.ObjectId, ref: 'Game'/* , required: true TODO stats by game */ },
  average: String,
  atBats: Number,
  hits: Number,
  onBase: Number,
  homeRuns: Number,
  runsBattedIn: Number,
  runs: Number,
  numErrors: Number,
  strikeouts: Number
});

statisticSchema.statics.getSeasons = async () => {
  try {
    const Statistic = mongoose.model('Statistic');
    const stats = await Statistic.find()
      .exec();
    const bySeason = (a, b) => {
      const aSeason = Number.parseInt(a.season.slice(-4), 10);
      const bSeason = Number.parseInt(b.season.slice(-4), 10);
      return aSeason - bSeason;
    };
    const seasons = [];
    stats.sort(bySeason)
      .forEach(stat => {
        const season = Number.parseInt(stat.season.slice(-4), 10);
        if (!seasons.includes(season)) seasons.push(season);
      });
    return seasons;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const Statistic = mongoose.model('Statistic', statisticSchema);

module.exports = Statistic;
