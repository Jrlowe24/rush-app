const mongoose = require('mongoose');
const Meta = require('./meta');

const { Schema } = mongoose;

const playerSchema = new Schema({
  authId: { type: Number, required: true },
  username: { type: String, required: true },
  name: { type: String, required: true },
  isCaptain: { type: Boolean, required: true },
  inDraft: { type: Boolean, required: true },
  isBCAdmin: { type: Boolean, required: true },
  draftPick: Number,
  statistics: [{ type: Schema.Types.ObjectId, ref: 'Statistic' }],
  teams: [{ type: Schema.Types.ObjectId, ref: 'Team' }]
});

playerSchema.statics.draft = async (captainAuthId, playerId) => {
  const Player = mongoose.model('Player');
  const Team = mongoose.model('Team');
  try {
    const meta = await Meta.findOne()
      .exec();
    const captain = await Player.findOne({ authId: captainAuthId })
      .exec();
    const team = await Team.findOne({ captain: captain._id, season: meta.season })
      .exec();
    const player = await Player.findById(playerId)
      .exec();
    player.teams.push(team._id);
    await player.save();
    team.players.push(playerId);
    return await team.save();
  } catch (err) {
    console.log(err);
    throw err;
  }
};

const Player = mongoose.model('Player', playerSchema);

module.exports = Player;
