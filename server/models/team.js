const mongoose = require('mongoose');
const Player = require('./player');
const addRefs = require('../util/addRefs');

const { Schema } = mongoose;

// Want it to fit the format (Fall, Spring, or Summer) **** (where * is a digit)
const validateSeason = val => /^(?:Fall|Spring|Summer) \d{4}$/.test(val);

const teamSchema = new Schema({
  name: String,
  captain: { type: Schema.Types.ObjectId, ref: 'Player', required: true },
  season: { type: String, required: true, validate: { validator: validateSeason } },
  players: [{ type: Schema.Types.ObjectId, ref: 'Player', required: true }]
});

teamSchema.statics.make = async (name, captain, season, players) => {
  const Team = mongoose.model('Team');
  try {
    const team = await Team.create({
      name,
      captain,
      season,
      players
    });
    const mapIdToPromise = id => addRefs(Player.findById(id).exec(), 'teams', team._id, true);
    await Promise.all(players.map(mapIdToPromise));
    return team;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

teamSchema.statics.getById = async id => {
  const Team = mongoose.model('Team');
  try {
    return await Team.findById(id)
      .populate('captain')
      .populate('players')
      .exec();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

teamSchema.statics.updateById = async (id, update, opts) => {
  const Team = mongoose.model('Team');
  try {
    return await Team.findByIdAndUpdate(id, update, opts)
      .exec();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

teamSchema.statics.removeById = async id => {
  const Team = mongoose.model('Team');
  try {
    return await Team.findByIdAndRemove(id)
      .exec();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const Team = mongoose.model('Team', teamSchema);

module.exports = Team;
