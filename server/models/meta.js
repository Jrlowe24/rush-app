const mongoose = require('mongoose');

const { Schema } = mongoose;

const metaSchema = new Schema({
  draft: { type: Boolean, required: true },
  draftPick: { type: Number, required: true },
  draftPicks: [{ type: Number, required: true }],
  season: { type: String, required: true },
  seasonInProgress: { type: Boolean, required: true }
});

const Meta = mongoose.model('Meta', metaSchema);

module.exports = Meta;
