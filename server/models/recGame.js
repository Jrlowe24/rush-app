const mongoose = require('mongoose');

const { Schema } = mongoose;

const uniqueDate = async date => {
  const RecGame = mongoose.model('RecGame');
  const dateFilter = game => {
    return game.date === date;
  };
  const games = await RecGame.find()
    .exec();
  if (games.filter(dateFilter).length > 0) {
    return new Error('Game with date exists');
  }
  return true;
};
const recGameSchema = new Schema({
  date: { type: String, validate: { validator: uniqueDate }, required: true },
  players: [{ type: String, required: true }],
  alts: [{ type: String }],
  creator: { type: String, required: true }
});

recGameSchema.statics.make = async (date, players, alts, creator) => {
  const RecGame = mongoose.model('RecGame');
  try {
    const game = new RecGame({
      date,
      players,
      alts,
      creator
    });
    return await game.save();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

recGameSchema.statics.deleteMany = async ids => {
  const RecGame = mongoose.model('RecGame');
  try {
    const promises = [];
    for (let i = 0; i < ids.length; i += 1) {
      promises.push(RecGame.findByIdAndRemove(ids[i]));
    }
    await Promise.all(promises);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

recGameSchema.methods.setDate = async function setDate(date) {
  try {
    await this.update({ date });
    return true;
  } catch (err) {
    console.error(err);
    return err;
  }
};

recGameSchema.methods.addPlayer = async function addPlayer(player) {
  try {
    if (!this.players.includes(player)) {
      this.players.push(player);
      await this.save();
    }
    return true;
  } catch (err) {
    console.error(err);
    return err;
  }
};

recGameSchema.methods.removePlayer = async function removePlayer(player) {
  try {
    const temp = [];
    for (let i = 0; i < this.players.length; i += 1) {
      if (this.players[i] !== player) {
        temp.push(this.players[i]);
      }
    }

    while (this.players.length < 8 && this.alts.length > 0) {
      temp.push(this.alts[0]);
      this.alts = this.alts.slice(1);
    }

    this.players = temp;
    await this.save();
    return true;
  } catch (err) {
    console.error(err);
    return err;
  }
};

recGameSchema.methods.addAlt = async function addAlt(alt) {
  try {
    if (!this.alts.includes(alt)) {
      this.alts.push(alt);
      await this.save();
    }
    return true;
  } catch (err) {
    console.error(err);
    return err;
  }
};

recGameSchema.methods.removeAlt = async function removeAlt(alt) {
  try {
    const temp = [];
    for (let i = 0; i < this.alts.length; i += 1) {
      if (this.alts[i] !== alt) {
        temp.push(this.alts[i]);
      }
    }
    this.alts = temp;
    await this.save();
    return true;
  } catch (err) {
    console.error(err);
    return err;
  }
};

const RecGame = mongoose.model('RecGame', recGameSchema);

module.exports = RecGame;
