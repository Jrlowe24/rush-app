const mongoose = require('mongoose');

const { Schema } = mongoose;

const gameSchema = new Schema({
  _id: Schema.Types.ObjectId,
  datePlayed: { type: Date, required: true },
  homeTeam: { type: Schema.Types.ObjectId, ref: 'Team' },
  awayTeam: { type: Schema.Types.ObjectId, ref: 'Team' },
  homeRoster: [{ type: Schema.Types.ObjectId, ref: 'Player' }],
  awayRoster: [{ type: Schema.Types.ObjectId, ref: 'Player' }],
  homeScore: { type: Number, required: true },
  awayScore: { type: Number, required: true }
});

const Game = mongoose.model('Game', gameSchema);

module.exports = Game;
