const Player = require('../models/player');
const Statistic = require('../models/statistic');
const Team = require('../models/team');
const Meta = require('../models/meta');

const getPlayersBySeason = (req, res) => {
  const { season } = req.query;

  Statistic.find({ season: `Spring ${season}` })
    .exec()
    .then(async stats => {
      try {
        let players = await Promise.all(
          stats.map(stat => Player.findById(stat.player)
            .populate('statistics')
            .populate('teams')
            .exec())
        );
        players = players.map(player => {
          const statistics = player.statistics.filter(stat => stat.season === `Spring ${season}`);
          return Object.assign(player, { statistics });
        });
        return res.json({
          success: true,
          players
        });
      } catch (err) {
        console.error(err);
        return res.json({
          success: false,
          msg: err
        });
      }
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.getPlayers = (req, res) => {
  const { season } = req.query;
  if (season) getPlayersBySeason(req, res);
  else {
    Player.find()
      .exec()
      .then(async players => {
        try {
          const playersWithStats = await Promise.all(
            players.map(player => player.populate('statistics')
              .populate('teams')
              .execPopulate())
          );
          return res.json({
            success: true,
            players: playersWithStats
          });
        } catch (err) {
          console.error(err);
          return res.json({
            success: false,
            msg: err
          });
        }
      })
      .catch(err => {
        console.error(err);
        return res.json({
          success: false,
          msg: err
        });
      });
  }
};

module.exports.getSeasons = (req, res) => {
  Statistic.getSeasons()
    .then(seasons => {
      return res.json({
        success: true,
        seasons
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.getPlayer = (req, res) => {
  const authId = req.session.user_id;
  Player.findOne({ authId })
    .populate('statistics')
    .populate('teams')
    .exec()
    .then(player => {
      return res.json({
        success: true,
        player
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.getPlayerTeams = async (req, res) => {
  const authId = req.session.user_id;
  try {
    const player = await Player.findOne({ authId })
      .exec();
    const teams = [];
    await Promise.all(player.teams.map(async teamId => teams.push(await Team.findById(teamId)
      .populate('players')
      .exec())));
    return res.json({
      success: true,
      teams
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.toggleDraft = (req, res) => {
  const authId = req.session.user_id;
  Player.findOne({ authId })
    .populate('statistics')
    .populate('teams')
    .exec()
    .then(player => {
      player.inDraft = !player.inDraft; // eslint-disable-line
      player.save()
        .then(newPlayer => {
          return res.json({
            success: true,
            player: newPlayer
          });
        })
        .catch(err => {
          console.error(err);
          return res.json({
            success: false,
            msg: err
          });
        });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.draftPlayers = async (req, res) => {
  const { usernames } = req.body;
  try {
    await Promise.all(usernames.map(async u => Player.findOneAndUpdate({ username: u }, { inDraft: true })
      .exec()));
    return res.json({
      success: true,
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.getMeta = (req, res) => {
  Meta.findOne()
    .exec()
    .then(meta => {
      return res.json({
        success: true,
        meta
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.startDraft = (req, res) => {
  Meta.findOneAndUpdate({}, { draft: true }, { new: true })
    .exec()
    .then(meta => {
      return res.json({
        success: true,
        meta
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.endDraft = async (req, res) => {
  try {
    const meta = await Meta.findOneAndUpdate({}, { draft: false, draftPick: 0, seasonInProgress: true }, { new: true })
      .exec();
    const players = await Player.find({ inDraft: true })
      .exec();
    await Promise.all(players.map(async player => {
      player.inDraft = false; // eslint-disable-line
      await player.save();
    }));
    return res.json({
      success: true,
      meta
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.addCaptain = async (req, res) => {
  const { id } = req.body;
  try {
    const meta = await Meta.findOne()
      .exec();
    const captain = await Player.findById(id)
      .exec();
    const captains = await Player.find({ isCaptain: true })
      .exec();
    captain.isCaptain = true;
    captain.draftPick = captains.length;
    await captain.save();
    const draftPicks = [];
    for (let i = 0; i < captains.length + 1; i += 1) draftPicks.push(i);
    for (let i = captains.length; i >= 0; i -= 1) draftPicks.push(i);
    meta.draftPicks = draftPicks;
    await meta.save();
    await Team.make('Unnamed', id, meta.season, [captain]);
    return res.json({
      success: true
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.removeCaptain = async (req, res) => {
  const { id } = req.body;
  try {
    const meta = await Meta.findOne()
      .exec();
    const player = await Player.findById(id)
      .populate('teams')
      .exec();
    player.isCaptain = false;
    player.draftPick = -1;
    const teamId = player.teams.filter(team => team.season === meta.season)[0]._id;
    player.teams = player.teams.filter(team => team._id.toString() !== teamId.toString());
    await player.save();
    await Team.removeById(teamId);
    return res.json({
      success: true
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.draftPlayer = async (req, res) => {
  const { id } = req.body;
  const authId = req.session.user_id;
  // const { authId } = req.body;
  try {
    await Player.draft(authId, id);
    const meta = await Meta.findOne()
      .exec();
    meta.draftPick = (meta.draftPick + 1) % meta.draftPicks.length;
    await meta.save();
    return res.json({
      success: true
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.getDraftPick = (req, res) => {
  Meta.findOne()
    .exec()
    .then(meta => {
      return res.json({
        success: true,
        draftPick: meta.draftPicks[meta.draftPick]
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.getDraftees = async (req, res) => {
  try {
    const meta = await Meta.findOne()
      .exec();
    const captains = await Player.find({ isCaptain: true })
      .populate('teams')
      .exec();
    const draftees = {};
    await Promise.all(captains.map(async captain => {
      const team = await Team.findOne({ captain: captain._id, season: meta.season })
        .populate('players')
        .exec();
      draftees[captain._id.toString()] = team.players.filter(player => player._id.toString() !== captain._id.toString());
    }));
    return res.json({
      success: true,
      draftees
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      msg: err
    });
  }
};

module.exports.getDeclared = (req, res) => {
  Player.find({ inDraft: true })
    .exec()
    .then(declared => {
      return res.json({
        success: true,
        declared
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};
