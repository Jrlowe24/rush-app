const request = require('request-promise-native');

module.exports.getUsers = (req, res) => {
  const options = {
    method: 'GET',
    uri: 'https://auth.pikapp.net/api/users',
    headers: {
      Accept: 'application/json',
      Authorization: process.env.AUTH_KEY,
      'Content-Type': 'application/json'
    },
    json: true
  };
  request(options)
    .then(json => {
      return res.json({
        success: true,
        users: json.users
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};
