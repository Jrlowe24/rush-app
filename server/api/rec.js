const RecGame = require('../models/recGame');

module.exports.getMonthData = (req, res) => {
  const monthFilter = game => {
    const date = new Date(game.date);
    const today = new Date(Date.now());
    const month = Number.parseInt(req.query.month, 10);
    const months = [month - 1, month, month + 1];
    return date.getFullYear() === today.getFullYear() && months.includes(date.getMonth());
  };

  RecGame.find()
    .exec()
    .then(games => {
      if (games) {
        const monthGames = games.filter(monthFilter);
        const map = {};

        monthGames.forEach(game => {
          const {
            date, players, alts, _id, creator
          } = game;
          const inGame = players.includes(req.session.username);
          const isCreator = req.session.username === creator;

          if (map[new Date(date).toDateString()]) {
            map[new Date(date).toDateString()].push({
              date, players, alts, _id, inGame, isCreator
            });
          } else {
            map[new Date(date).toDateString()] = [{
              date, players, alts, _id, inGame, isCreator
            }];
          }
        });
        return res.json({
          success: true,
          data: map
        });
      }

      return res.json({
        success: true,
        data: {}
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.allGames = (req, res) => {
  RecGame.find()
    .exec()
    .then(games => {
      return res.json({
        success: true,
        games
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.createGame = (req, res) => {
  let creator = req.session.username;
  if (!creator) { // TODO change this - temp fix for undefined username
    creator = req.body.players[0]; // eslint-disable-line
  }
  RecGame.make(req.body.date, req.body.players, req.body.alts, creator)
    .then(game => {
      if (game) {
        return res.json({
          success: true,
          gameId: game._id
        });
      }
      return res.json({
        success: false,
        msg: 'Game not validated'
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.deleteGame = (req, res) => {
  RecGame.findByIdAndDelete(req.body.gameId)
    .exec()
    .then(() => res.json({
      success: true,
      msg: `DELETE RecGame ${req.body.gameId}`
    }))
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.deleteGames = (req, res) => {
  RecGame.deleteMany(req.body.gameIds)
    .then(() => res.json({
      success: true,
      msg: `DELETE RecGames ${req.body.gameIds}`
    }))
    .catch(err => res.json({
      success: false,
      msg: err
    }));
};

module.exports.setDate = (req, res) => {
  RecGame.findById(req.body.gameId)
    .setDate(req.body.date)
    .then(() => {
      return res.json({
        success: true
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.addPlayer = (req, res) => {
  RecGame.findById(req.body.gameId)
    .then(game => {
      game.addPlayer(req.session.username)
        .then(() => {
          return res.json({
            success: true
          });
        })
        .catch(err => {
          return res.json({
            success: false,
            msg: err
          });
        });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.removePlayer = (req, res) => {
  RecGame.findById(req.body.gameId)
    .then(game => {
      game.removePlayer(req.session.username)
        .then(() => {
          return res.json({
            success: true
          });
        })
        .catch(err => {
          return res.json({
            success: false,
            msg: err
          });
        });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.addAlt = (req, res) => {
  RecGame.findById(req.body.gameId)
    .addAlt(req.body.alt)
    .then(() => {
      return res.json({
        success: true
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.removeAlt = (req, res) => {
  RecGame.findById(req.body.gameId)
    .removeAlt(req.body.alt)
    .then(() => {
      return res.json({
        success: true
      });
    })
    .catch(err => {
      console.log(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};
