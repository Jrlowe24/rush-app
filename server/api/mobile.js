const isMobile = require('ismobilejs');

module.exports.detectMobile = (req, res) => {
  const userAgent = req.headers['user-agent'];
  return res.json({
    success: true,
    isMobile: isMobile(userAgent).any
  });
};
