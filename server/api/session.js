module.exports.logout = (req, res) => {
  req.session.destroy();
  if (!req.query.fromAuth || req.query.fromAuth === 'false') {
    if (process.env.NODE_ENV !== 'production') {
      res.redirect('http://localhost:3100/logout?ref=http://localhost:3000/logout');
    } else {
      res.redirect('https://auth.pikapp.net/logout?ref=https://beercan.pikapp.net/logout');
    }
  } else {
    res.redirect('https://auth.pikapp.net/loggedout?logoutCompleted=true');
  }
};
