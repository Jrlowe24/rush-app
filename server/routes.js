const path = require('path');
const session = require('./api/session');
const league = require('./api/league');
const rec = require('./api/rec');
const authApi = require('./api/auth');
const auth = require('./util/authentication');
const mobile = require('./api/mobile');

module.exports = function routes(app) {
  app.get('/logout', session.logout);

  app.get('/api/league/players', league.getPlayers);
  app.get('/api/league/player', league.getPlayer);
  app.get('/api/league/player/teams', league.getPlayerTeams);
  app.get('/api/league/player/draft', league.toggleDraft);
  app.post('/api/league/draftPlayers', league.draftPlayers);
  app.get('/api/league/info', league.getMeta);
  app.post('/api/league/draft', league.startDraft);
  app.get('/api/league/endDraft', league.endDraft);
  app.post('/api/league/addCaptain', league.addCaptain);
  app.post('/api/league/removeCaptain', league.removeCaptain);
  app.post('/api/league/draftPlayer', league.draftPlayer);
  app.get('/api/league/draftees', league.getDraftees);
  app.get('/api/league/declared', league.getDeclared);
  app.get('/api/league/draftPick', league.getDraftPick);
  app.get('/api/league/seasons', league.getSeasons);

  app.get('/api/mobile', mobile.detectMobile);

  app.get('/api/rec/users', authApi.getUsers);
  app.get('/api/rec/monthData/:month?', rec.getMonthData);
  app.get('/api/rec/allGames', rec.allGames);
  app.post('/api/rec/createGame', rec.createGame);
  app.delete('/api/rec/deleteGame', rec.deleteGame);
  app.delete('/api/rec/deleteGames', rec.deleteGames);
  app.post('/api/rec/setDate', rec.setDate);
  app.post('/api/rec/addPlayer', rec.addPlayer);
  app.post('/api/rec/removePlayer', rec.removePlayer);
  app.post('/api/rec/addAlt', rec.addAlt);
  app.post('/api/rec/removeAlt', rec.removeAlt);

  app.get('/status', (req, res) => res.status(200).send('OK'));

  app.get('/*', auth.checkAuth, (req, res) => {
    res.sendFile(path.join(__dirname, '../public/app.html'));
  });
};
