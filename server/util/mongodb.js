const mongoose = require('mongoose');

// Set native promises as mongoose promise
mongoose.Promise = global.Promise;

const mongoDB = process.env.MONGO_URL || 'mongodb://localhost:27017/beercan';
mongoose.connect(mongoDB, { useNewUrlParser: true });

// Get the default connection
const db = mongoose.connection;

// Bind connection to error event (to get notification of connection errors)
db.on('error', err => {
  console.error('A MongoDB connection error has occurred.');
  console.error(err);
  console.error('Application server is exiting with non-zero exit code to ensure server is removed from cluster');
  return process.exit(1);
});

module.exports = db;
