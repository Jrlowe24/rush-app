const addRefs = async (promise, property, id) => {
  const doc = await promise;
  if (!doc) {
    throw new Error('Document not found');
  }

  try {
    if (Array.isArray(doc[property])) {
      doc[property].push(id);
    } else {
      doc[property] = id;
    }
    return await doc.save();
  } catch (err) {
    throw err;
  }
};

module.exports = addRefs;
