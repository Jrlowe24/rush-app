const request = require('request-promise-native');
const Player = require('../models/player');

const options = {
  method: 'GET',
  uri: 'https://auth.pikapp.net/api/users',
  headers: {
    Accept: 'application/json',
    Authorization: process.env.AUTH_KEY,
    'Content-Type': 'application/json'
  },
  json: true
};
request(options)
  .then(json => {
    json.users.forEach(user => {
      Player.findOne({ authId: user.id })
        .exec()
        .then(player => {
          if (!player) {
            const newPlayer = new Player({
              authId: user.id,
              username: user.username.toLowerCase(),
              name: user.name,
              isCaptain: false,
              inDraft: false,
              isBCAdmin: false
            });
            newPlayer.save()
              .then(() => {
                console.log('Player imported from Auth');
              })
              .catch(console.log);
          } else if (player.name !== user.name || player.username !== user.username.toLowerCase()) {
            player.name = user.name; // eslint-disable-line
            player.username = user.username.toLowerCase(); // eslint-disable-line
            player.save()
              .then(() => {
                console.log('Player updated from Auth');
              })
              .catch(console.log);
          }
        })
        .catch(console.log);
    });
  })
  .catch(console.log);
