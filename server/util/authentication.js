let { DEBUG } = process.env;
DEBUG = String(DEBUG) === 'true';

function redirectToLogin(res) {
  let appID = '1312088';
  let redir = 'http://localhost:3000';
  let host = 'http://localhost:3100';

  if (process.env.NODE_ENV === 'production') {
    appID = process.env.SSO_APP_ID;
    redir = process.env.SSO_REDIRECT;
    host = process.env.SSO_HOSTNAME;
    if (!appID) {
      console.log('AppID not found in env');
    }
    if (!redir) {
      console.log('Redirect not found in env');
    }
    if (!host) {
      console.log('Host not found in env');
    }
  }
  return res.redirect(`${host}?appID=${appID}&continue=${encodeURI(redir)}`);
}

function ignoreAuth() {
  return String(process.env.IGNORE_AUTH)
    .toLowerCase() === 'true';
}

function setDummyAuth(req, next) {
  req.session.user_id = 0;
  req.session.username = 'dev';
  req.session.email = 'test@dev.com';
  req.session.admin = true;
  req.session.manager = true;
  req.session.save(() => {
    return next();
  });
}

function checkForSessionResponse(req) {
  return req.query.session;
}

function setSession(req, res, done) {
  const sessionData = JSON.parse(new Buffer(req.query.session, 'base64').toString()); // eslint-disable-line
  req.session.user_id = sessionData.user_id;
  req.session.username = sessionData.username;
  req.session.email = sessionData.email;
  req.session.admin = sessionData.admin;
  req.session.manager = sessionData.manager;
  if (DEBUG) console.log(`Setting session for ${sessionData.username} (${sessionData.user_id})`);
  req.session.save(err => {
    if (err) {
      console.error(err);
      return res.send('An error occurred while setting your session');
    }
    if (DEBUG) console.log('Session saved successfully');
    return done();
  });
}

function sendAPIAuthFailed(res) {
  const json = {
    error: true,
    message: 'You did not provide a valid token parameter'
  };
  return res.status(403).json(json);
}

module.exports.checkAuth = function checkAuth(req, res, next) { // eslint-disable-line
  if (ignoreAuth()) return setDummyAuth(req, next);

  if (checkForSessionResponse(req) === true) {
    if (DEBUG) console.log('Got session... parsing and setting');
    setSession(req, res, () => {
      if (DEBUG) console.log('Session set successfully');
      return res.redirect('/');
    });
  } else {
    if (!req.session.user_id) {
      if (DEBUG) console.log('No session found, redirecting to Auth application');
      return redirectToLogin(res);
    }
    if (DEBUG) console.log(`Valid user session ${req.session.username} (${req.session.user_id})`);
    return next();
  }
};

module.exports.checkAPI = function checkAPI(req, res, next) {
  if (process.env.DEBUG) {
    console.log('[DEBUG] Running in DEBUG mode... skipping API authentication');
    return next();
  }

  if (req.query.token) {
    if (process.env.API_TOKEN) {
      if (req.query.token === process.env.API_TOKEN) {
        return next();
      }
      return sendAPIAuthFailed(res);
    }
    return res.status(500).json({
      error: true,
      message: 'An internal server error has occurred'
    });
  }
  return sendAPIAuthFailed(res);
};

module.exports.checkAdmin = function checkAdmin(req, res, next) {
  if (!req.session.user_id) {
    req.flash('error', 'You must be logged in to view that page');
    return res.redirect('/login');
  } if (!req.session.user_id || req.session.admin !== true) {
    req.flash('error', 'You don\'t have permission to view that page');
    return res.redirect('back');
  }
  if (req.session.admin === true) {
    return next();
  }
  req.flash('error', 'You don\'t have permission to view that page');
  return res.redirect('back');
};

module.exports.checkManager = function checkManager(req, res, next) {
  if (!req.session.user_id) {
    req.flash('error', 'You must be logged in to view that page');
    return res.redirect('/login');
  } if (!req.session.manager || req.session.manager !== true) {
    req.flash('error', 'You don\'t have permission to view that page');
    return res.redirect('back');
  }
  return next();
};

module.exports.checkNotAuth = function checkNotAuth(req, res, next) {
  if (req.session.user_id) {
    return res.redirect('/');
  }
  return next();
};
