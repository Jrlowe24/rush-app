import Team from '../../../server/models/team';
import Player from '../../../server/models/player';
import mongoose from 'mongoose'; // Jest auto-mocks if there is a mock with the same name in __mocks__ so we're really importing __mocks__/mongoose

describe('Team', () => {
  mongoose.Promise = global.Promise;

  beforeAll(async () => {
    return await mongoose.connect();
  });

  afterAll(async () => {
    return await mongoose.disconnect();
  });

  let teamId = new mongoose.Types.ObjectId();
  let captainId = new mongoose.Types.ObjectId();
  let playerIds = [
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
  ];

  beforeAll(async () => {
    const promises = [
      // Persist mock db with mock data
      Team.create({
        _id: teamId,
        captain: captainId,
        season: 'Fall 2017',
        players: playerIds,
      }),
      // authId properties here for something to test against
      Player.create(
        {
          _id: captainId,
          authId: 123,
        },
        {
          _id: playerIds[0],
          authId: 456,
        },
        {
          _id: playerIds[1],
          authId: 789,
        }
      ),
    ];

    return await Promise.all(promises);
  });

  describe('make', () => {
    it("should create the team if it has a valid season and year and add its id to its linked Players' teams field", async done => {
      await Player.find({}).updateMany({ teams: [] }); // Update Players with empty teams property
      await Team.make('Team', captainId, 'Fall 2017', playerIds);
      expect.assertions(7);
      return await Team.findOne({ name: 'Team' })
        .populate('captain')
        .populate('players')
        .exec()
        .then(doc => {
          expect(doc.season).toBe('Fall 2017');
          expect(doc.captain.authId).toBe(123);
          expect(doc.players.length).toEqual(2);
          expect(doc.players[0].authId).toBe(456);
          expect(doc.players[1].authId).toBe(789);
          expect(doc.players[0].teams).toContain(doc._id);
          expect(doc.players[1].teams).toContain(doc._id);
          done();
        });
    });

    it("shouldn't create the team if it doesn't have a valid season", async done => {
      await Player.find({}).updateMany({ teams: [] }); // Update Players with empty teams property
      spyOn(console, 'error');
      expect.assertions(2);
      await expect(
        Team.make('Team 2', captainId, 'Winter 2017', playerIds)
      ).rejects.toBeInstanceOf(mongoose.Error.ValidationError);
      expect(console.error).toHaveBeenCalled();
      done();
    });

    it("shouldn't create the team if it doesn't have a valid year", async done => {
      await Player.find({}).updateMany({ teams: [] }); // Update Players with empty teams property
      spyOn(console, 'error');
      expect.assertions(2);
      await expect(
        Team.make('Team 2', captainId, 'Fall ABCD', playerIds)
      ).rejects.toBeInstanceOf(mongoose.Error.ValidationError);
      expect(console.error).toHaveBeenCalled();
      done();
    });
  });

  describe('getById', () => {
    it('should retrieve the corresponding document and populate its fields if it exists', done => {
      Team.getById(teamId).then(doc => {
        expect(doc.season).toBe('Fall 2017');
        expect(doc.captain.authId).toBe(123);
        expect(doc.players.length).toEqual(2);
        expect(doc.players[0].authId).toBe(456);
        expect(doc.players[1].authId).toBe(789);
        done();
      });
    });

    it("should return null if the corresponding document doesn't exist", async done => {
      expect.assertions(1);
      await expect(Team.getById(new mongoose.Types.ObjectId())).resolves.toBe(
        null
      );
      done();
    });
  });

  describe('updateById', () => {
    it('should retrieve the corresponding document and update its fields if it exists', async done => {
      expect.assertions(1);
      await Team.updateById(
        teamId,
        { season: 'Spring 2018' },
        { new: true }
      ).then(doc => {
        expect(doc.season).toBe('Spring 2018');
      });
      done();
    });

    it("should return null if the corresponding document doesn't exist", async done => {
      expect.assertions(1);
      await expect(Team.getById(new mongoose.Types.ObjectId())).resolves.toBe(
        null
      );
      done();
    });
  });

  describe('removeById', () => {
    it('should remove the document from the database if it exists', async done => {
      expect.assertions(1);
      await Team.removeById(teamId);
      await expect(Team.findById(teamId)).resolves.toBe(null);
      done();
    });

    it("should return null if the corresponding document doesn't exist", async done => {
      expect.assertions(1);
      await expect(Team.removeById(teamId)).resolves.toBe(null);
      done();
    });
  });
});
