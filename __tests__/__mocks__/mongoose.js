import mongoose from 'mongoose';
import MongodbMemoryServer from 'mongodb-memory-server';

mongoose.Promise = global.Promise;

const originalConnect = mongoose.connect;
mongoose.connect = (async () => {
  const mongoServer = new MongodbMemoryServer();

  const mongoUri = await mongoServer.getConnectionString(true);

  const opts = {
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    useNewUrlParser: true
  };

  originalConnect.bind(mongoose)(mongoUri, opts);

  mongoose.connection.on('error', e => {
    if (e.message.code === 'ETIMEDOUT') {
      console.error(e);
    } else {
      throw e;
    }
  });

  mongoose.connection.once('open', () => {
    console.log(`MongoDB successfully connected to ${mongoUri}`);
  });

  mongoose.connection.once('disconnected', () => {
    console.log('MongoDB disconnected!');
    mongoServer.stop();
  });
});

const originalModel = mongoose.model;
mongoose.model = (name, schema) => originalModel.bind(mongoose)(name, schema, null, true);

export default mongoose;
