# Helpful Git commands
* `git checkout -b <branch_name> master`: Create and checkout (switch to) branch <branch_name> (name this something relevant to the issue i.e. webpack) based off of master (essentially creating a copy at this point)
* `git add <files>`: Add the changes you have made to your files so that git knows you want to stage them for a commit
* `git commit`: Commit the changes you have made to the files you have staged. This will, due to the "pre-commit" hook in package.json, lint and test your code. Try not to skip the checks because you likely don't want to introduce breaking code.
* `git commit -n`: Commit the staged changes without running the pre-commit hook, which is set up to lint and test
* `git push -u origin HEAD`: Sets the local branch to track (due to the -u flag, which is an alias for --set-upstream) a remote branch of the same name and push those changes to the remote repository (aka origin). After you specify -u once, you can omit it the following times as long as it's for the same branch
#### Updating your local repo
I separated this from the rest of the commands because there is a fair amount of debate surrounding it, which I will explain below
* `git pull --rebase`: Execute the following two commands as one
    1. `git fetch`: Update your local repo's refs for this branch so it knows that there are changes in the remote repo
    2. `git rebase origin/<branch_name>`:  Rebase the currently checked-out branch against the remote-tracking branch <branch_name> (so if you are on a branch called dev which tracks origin/dev, then you would do git rebase origin/dev), which fast-forwards your local history
* `git pull`: Execute the following two commands as one
    1. `git fetch`: Update your local repo's refs for this branch so it knows that there are changes in the remote repo
    2. `git merge origin/<branch_name>`: Merge the remote-tracking branch <branch_name> into the currently checked-out branch (so if you are on a branch called dev which tracks origin/dev, then you would do git merge origin/dev), which combines your local history
* `git fetch --all`: Update your local repo's refs for all branches

The first method is (arguably) preferable to the second for personal branches because it avoids unnecessary merge commits and has a more (again, arguably) intuitive working history. **However**, if you do this for a shared branch such as master, you can end up rewriting your collaborators' history, which is not ideal.
#### In summary
##### Preferring a rebase *is* only a (subjective) best practice, so take it as you will
* For branches that only you are working on (e.g. you create a branch to resolve an issue which has been assigned to you), use `git fetch --all` and `git rebase origin/<branch_name>` or `git pull --rebase`
* For shared branches, such as **master**, to be safe, ***always*** use `git pull`, ***never*** a rebase
