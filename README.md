# Pi Kappa Phi Beercan System
This is the codebase for [Beercan](https://beercan.pikapp.net), a system designed
to provide users with useful services for all beercan-related activities.

## Dependencies
On macOS Or Linux, install these with the package manager of your choice
(i.e. apt-get, yum) instead of brew, which you can install [here](https://brew.sh/), with the form `brew install 'pkg'`.
On Windows, or if you don't want to use a package manager on macOS or Linux, follow the links and download them.
* [Git](https://git-scm.com/download/win)
* [Node.js](https://nodejs.org/en/download/)
* [MongoDB Community Server **(not Atlas, Enterprise Server, etc.)**](https://www.mongodb.com/download-center?#community)
* [Node-gyp dependency based on OS](https://github.com/nodejs/node-gyp#installation)
* [Redis server macOS/Linux](http://download.redis.io/releases/redis-5.0.3.tar.gz)
* [Redis server Windows](https://github.com/MicrosoftArchive/redis/releases/download/win-3.2.100/Redis-x64-3.2.100.msi)
### If you download the MongoDB server from the website, then you will need to add it to your path, which is an environment variable which points your system to the specified directories to search for executable files
* On Windows, run this from your command line: `setenv.bat <path-to-mongo-installation-bin>` e.g. `setenv.bat C:\Program Files\MongoDB\Server\3.6\bin\`
* On macOS or Linux, run this from your terminal: `./setenv.sh` (**Note this is not necessary if you used a package manager as it automatically configures your PATH for you**)

## Getting Started
1. Make a folder called PiKapp (**don't use spaces** in your file and folder names, because the console will read the space and then try to parse everything after it as a separate argument, rather than the rest of your file/folder path, forcing you to unnecessarily always surround your path with quotes) and cd into it with `mkdir ~/PiKapp && cd ~/PiKapp`
2. Clone this repository into a subdirectory _/Beercan_ and cd into it with
`git clone https://github.com/GTPiKapp/Beercan && cd Beercan/`
3. Run `npm install` to install all of the Node modules specified as dependencies
or devDependencies in _package.json_, which can be found in _node_modules/_
4. Duplicate the `.env.example` file and name it `.env`, which is where the dotenv package pulls our environment variables from. You can access these variables anywhere in your code using `process.env.KEY`. The `.env` file stores each environment variable as a `KEY=VALUE` pair on its own line. It's very important you do _not_ commit the `.env` file to the repository, as that will contain passwords and access tokens for our production environment.
5. Set up the necessary data directory for MongoDB using one of the commands
    * macOS/Linux: `sudo mkdir -p /data/db`
    * Windows: Open a separate command line with Administrator Privileges (press the `Win` key, type `cmd.exe` without pressing `Enter`, then press `Ctrl + Shift + Enter`), and then run `mkdir \data\db`
6. Open a separate terminal (or if you opened a separate command line with Admin Privileges in the previous step, use that command line) and launch an instance of the MongoDB server with
`mongod`
7. If this command fails with an error about a read-only directory (you'll have to
read through its output, it's not at the bottom), run the following command to
change the directory's ownership `sudo chown -R $USER /data/db` and then try the
previous command again
8. Now to be able to log in to Beercan using the Auth server, there is a lot of complicated necessary setup involved, which is a lot better shown in person. So, for the time being, add an environment variable in the .env file as following: `IGNORE_AUTH=true`. This allows us to bypass the Auth server and instead, our app sets a dummy user session.
9. In order for this dummy session to be set, we need to open yet another console and launch the redis database with the command `redis-server` and allow it to run in the background.
10. Now return to the other terminal. At this point, in the background, you should have a mongo server running (for storing our application's data) and a redis server running (for storing our user session data). Now, type `npm run start:dev`. This script lints (checking for certain rules about code style, usage, and best practices) your code
and then upon adherence to the rules launches a Node monitor (nodemon) server in development mode
which watches your files for changes and automatically refreshes. Look to
_package.json_ under _"scripts"_ for more scripts to run, such as `build` and `test`
10. Now open up the browser of your choice and navigate to http://localhost:3000
and you're ready to go!

## Helpful Terminal or DOS (aka cmd, command line) commands
Can also be found [here](./TERMINAL.md)  
#### Key: ml: macOS/Linux, w: Windows
* `.`: Refers to the working directory
* `..`: Refers to the parent directory
* `cd <dir_path>` (ml, w): **C**hange **d**irectory to the one specified by <dir_path>, which essentially means that you are 'opening' this folder in your terminal
* `ls` (ml) or `dir` (w): **L**i**s**t the files available in the current **dir**ectory
* `pwd` (ml): **P**rint **w**orking **d**irectory; you don't need this for Windows because your terminal should most likely show the full path at the prompt anyway
* `rm <file_path>` (ml) or `del <file_path>` (w): **R**e**m**ove or **del**ete the file specified by <file_path>
* `rm -rf <dir_path>` (ml) or `rmdir /s /q <dir_path>` (w): **R**e**m**ove the directory specified by <dir_path>, as well as all of the subdirectories and files which belong to it
* `<cmd_1> && <cmd_2> && ...` (ml, w): Chain <cmd_1>, <cmd_2>, **and** (&&) all following commands (minimum of 2 total) together, which saves you from typing the commands out on separate lines, but executes it the same way - one after the other  
**While you are typing out file/directory paths or some commands, you can press `TAB` to complete it for you *(if you have already typed out enough so that it can distinguish it from other files at that path or other commands)* so you don't have to type out the whole thing**

## Helpful Git commands
Can also be found [here](./GIT.md)
* `git checkout -b <branch_name> master`: Create and checkout (switch to) branch <branch_name> (name this something relevant to the issue i.e. webpack) based off of master (essentially creating a copy at this point)
* `git add <files>`: Add the changes you have made to your files so that git knows you want to stage them for a commit
* `git commit`: Commit the changes you have made to the files you have staged. This will, due to the "pre-commit" hook in package.json, lint and test your code. Try not to skip the checks because you likely don't want to introduce breaking code.
* `git commit -n`: Commit the staged changes without running the pre-commit hook, which is set up to lint and test
* `git push -u origin HEAD`: Sets the local branch to track (due to the -u flag, which is an alias for --set-upstream) a remote branch of the same name and push those changes to the remote repository (aka origin). After you specify -u once, you can omit it the following times as long as it's for the same branch
#### Updating your local repo
I separated this from the rest of the commands because there is a fair amount of debate surrounding it, which I will explain below
* `git pull --rebase`: Execute the following two commands as one
    1. `git fetch`: Update your local repo's refs for this branch so it knows that there are changes in the remote repo
    2. `git rebase origin/<branch_name>`:  Rebase the currently checked-out branch against the remote-tracking branch <branch_name> (so if you are on a branch called dev which tracks origin/dev, then you would do git rebase origin/dev), which fast-forwards your local history
* `git pull`: Execute the following two commands as one
    1. `git fetch`: Update your local repo's refs for this branch so it knows that there are changes in the remote repo
    2. `git merge origin/<branch_name>`: Merge the remote-tracking branch <branch_name> into the currently checked-out branch (so if you are on a branch called dev which tracks origin/dev, then you would do git merge origin/dev), which combines your local history
* `git fetch --all`: Update your local repo's refs for all branches

The first method is (arguably) preferable to the second for personal branches because it avoids unnecessary merge commits and has a more (again, arguably) intuitive working history. **However**, if you do this for a shared branch such as master, you can end up rewriting your collaborators' history, which is not ideal.
#### In summary
##### Preferring a rebase *is* only a (subjective) best practice, so take it as you will
* For branches that only you are working on (e.g. you create a branch to resolve an issue which has been assigned to you), use `git fetch --all` and `git rebase origin/<branch_name>` or `git pull --rebase`
* For shared branches, such as **master**, to be safe, ***always*** use `git pull`, ***never*** a rebase
